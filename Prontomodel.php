<?php

class Prontomodel extends CI_Model{

  function __construct()
  {
    parent::__construct();
  }

  function InsertVoto($data){

    $this->db->insert('votaciones',$data);

  }

  function InsertRegistro($id,$data){

    $this->db->where("id",$id);
    $this->db->update('votaciones',$data);

}

  function GetOption($opcion){
    $this->db->select("opcion");
    $this->db->from("votaciones");
    $this->db->where("opcion",$opcion);

    if($query = $this->db->get()){
      return $query->num_rows();
    }else{
      return false;
    }
  }

  function Getramdon($tipo){
    $this->db->order_by('tipo','RANDOM');
  //$this->db->select('frase');
    $this->db->where("tipo",$tipo);
    $this->db->limit(1);
    $query = $this->db->get('frases');
    return $query->row();
  }

  }
