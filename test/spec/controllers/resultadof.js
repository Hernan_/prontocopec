'use strict';

describe('Controller: ResultadofCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var ResultadofCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ResultadofCtrl = $controller('ResultadofCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ResultadofCtrl.awesomeThings.length).toBe(3);
  });
});
