'use strict';

describe('Controller: InscripcionfCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var InscripcionfCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InscripcionfCtrl = $controller('InscripcionfCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InscripcionfCtrl.awesomeThings.length).toBe(3);
  });
});
