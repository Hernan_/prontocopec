'use strict';

describe('Controller: EneristasCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var EneristasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EneristasCtrl = $controller('EneristasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EneristasCtrl.awesomeThings.length).toBe(3);
  });
});
