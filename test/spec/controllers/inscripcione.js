'use strict';

describe('Controller: InscripcioneCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var InscripcioneCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InscripcioneCtrl = $controller('InscripcioneCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InscripcioneCtrl.awesomeThings.length).toBe(3);
  });
});
