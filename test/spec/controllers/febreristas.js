'use strict';

describe('Controller: FebreristasCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var FebreristasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FebreristasCtrl = $controller('FebreristasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FebreristasCtrl.awesomeThings.length).toBe(3);
  });
});
