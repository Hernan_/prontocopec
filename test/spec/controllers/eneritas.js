'use strict';

describe('Controller: EneritasCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var EneritasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EneritasCtrl = $controller('EneritasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EneritasCtrl.awesomeThings.length).toBe(3);
  });
});
