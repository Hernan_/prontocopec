<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class Prontoctrl extends CI_Controller {

  public function generator(){

    $num1=mt_rand(0,25);
    $num2=mt_rand(0,25);
    $num3=mt_rand(0,25);
    $num4=mt_rand(0,25);
    $num5=mt_rand(1,9);
    $num6=mt_rand(1,9);

    $letra=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

    $codigo=''.$letra[$num1].''.$letra[$num2].''.$letra[$num3].''.$letra[$num4].''.$num5;


     $code = array("codigo" => "$codigo");
    header('Content-Type: application/json');
     echo json_encode($code);

    return $codigo;

  }

  public function prontoCopec(){

  $enero = 1;
  $febrero = 2;
  $porciento = 100;

  $data = array(
  'id' => $this->input->post('id'),
  'opcion' => $this->input->post('opcion'),
  );

  if ($data == null) {
    $this->_app_output($this->error);
  }
  else {
    $opcion = $data['opcion'];
    $this->load->model('Prontomodel');
    $this->Prontomodel->InsertVoto($data);

    $this->load->model('Prontomodel');
    $votoEnero = $this->Prontomodel->GetOption($enero);

    $this->load->model('Prontomodel');
    $votoFebrero = $this->Prontomodel->GetOption($febrero);

    $total = $votoEnero + $votoFebrero;
    $PorcentajeEnerista = $votoEnero * $porciento / $total;
    $PorcentajeFebrerista = $votoFebrero * $porciento / $total;



    if ($opcion==1) {

      if ($votoEnero>$votoFebrero) {

        $tipo = 1;
        $this->load->model('Prontomodel');
        $frasecelebre = $this->Prontomodel->Getramdon($tipo);

      }

    else {
        $tipo = 2;
        $this->load->model('Prontomodel');
        $frasecelebre = $this->Prontomodel->Getramdon($tipo);

      }
    }

     else  {

      if ($votoFebrero>$votoEnero) {
        $tipo = 3;
        $this->load->model('Prontomodel');
        $frasecelebre = $this->Prontomodel->Getramdon($tipo);

      }

      else {
        $tipo = 4;
        $this->load->model('Prontomodel');
        $frasecelebre = $this->Prontomodel->Getramdon($tipo);

      }
    }

        if ($votoFebrero==$votoEnero) {
        $tipo = 5;
        $this->load->model('Prontomodel');
        $frasecelebre = $this->Prontomodel->Getramdon($tipo);

      }

     $porcentajefull = array("PorcentajeEnerista" => "$PorcentajeEnerista", "PorcentajeFebrerista" => "$PorcentajeFebrerista");

     $todo = array('porce' => $porcentajefull, 'frase' => $frasecelebre);
     header('Content-Type: application/json');
     echo json_encode($todo);

  }


 }

 public function registro(){

   $id = $this->input->post('id');

   $data = array(
   'nombre' => $this->input->post('nombre'),
   'correo' => $this->input->post('correo'),
   'rut' => $this->input->post('rut'),
   'cond1' => $this->input->post('cond1'),
   'cond2' => $this->input->post('cond2'),

   );

   $this->load->model('Prontomodel');
   $this->Prontomodel->InsertRegistro($id,$data);

   if ($data==null) {
     $this->_app_output($this->error);
   }

   else {
     $satifaction = true;
     $thebether = array("registro" => "$satifaction");

     header('Content-Type: application/json');
     echo json_encode($thebether);
   }


 }

}
