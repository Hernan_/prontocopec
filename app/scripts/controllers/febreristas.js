'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:FebreristasCtrl
 * @description
 * # FebreristasCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('FebreristasCtrl', function (conection,$scope,$location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.body = {

      'background-image':'url("../images/Rosadoancho.png")',
      'background-size': 'cover',
      'height': '100vh',
      'background-attachment':'fixed',
      'overflow-y':'hidden'
    }

    conection.codeman({

   },function(response){

      var codigo = response.codigo;
      localStorage.setItem("code",codigo);
   });

  $scope.febrerista = function(){

       conection.porcentaje({

         id: localStorage.getItem("code"),
         opcion: 2

       },function(data){

         localStorage.setItem("frase",data.frase.frase);
         localStorage.setItem("enero",data.porce.PorcentajeEnerista);
         localStorage.setItem("febrero",data.porce.PorcentajeFebrerista);
         $location.path('/resultado-febrerista');

       });

     };
  });
