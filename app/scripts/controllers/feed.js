'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('FeedCtrl', function ($scope,matchmedia) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.phone = matchmedia.isPhone();


    if($scope.phone){

      $scope.body = {

        'background-image':'url("../images/fondo_mix_cel.png")',
        'background-size': 'contain',
        'height': '100vh',
        'background-attachment':'fixed'
      }
}

else{
  $scope.body = {

    'background-image':'url("../images/fondo_mix.png")',
    'background-size': 'cover',
    'height': '100vh',
    'background-attachment':'fixed'
}

}

  });
