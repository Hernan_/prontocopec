'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:InscripcionfCtrl
 * @description
 * # InscripcionfCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('InscripcionfCtrl', function ($scope,conection,$location,SweetAlert) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.body = {

      'background-image':'url("../images/Rosadoancho.png")',
      'background-size': 'cover',
      'height': '100vh',
      'background-attachment':'fixed',
      'overflow-y':'hidden'
    }

    $scope.formData = {};
    $scope.checkboxModel = {
      value1 : true,
      value2 : true
    };

    var terminos;
    var spam;

    if ($scope.checkboxModel.value1 ) {

       terminos = 1;
    }

    else {
       terminos = 2;
    }

    if ($scope.checkboxModel.value2) {

       spam = 1;
    }

    else {
       spam = 2;
    }

    $scope.region = [{nombre:'Región de Tarapacá'},
    {nombre:'Región de Antofagasta'},
    {nombre:'Región de Atacama'},
    {nombre:'Región de Coquimbo'},
    {nombre:'Región de Valparaíso'},
    {nombre:'Región del Libertador Gral. Bernardo O’Higgins'},
    {nombre:'Región del Maule'},
    {nombre:'Región del Biobío'},
    {nombre:'Región de la Araucanía'},
    {nombre:'Región de Los Lagos'},
    {nombre:'Región Aisén del Gral. Carlos Ibáñez del Campo'},
    {nombre:'Región de Magallanes y de la Antártica Chilena'},
    {nombre:'Región Metropolitana de Santiago'},
    {nombre:'Región de Los Ríos'},
    {nombre:'Región de Arica y Parinacota',}]

    $scope.send = function (formData) {

      var name = formData.nombre;
      var correo = formData.email;
      var rut = formData.rut;
      var region = formData.region;

      if (name == undefined || correo == undefined || rut == undefined || region == undefined) {

        console.log('campos vacios');
      }

      else {
        conection.registro({

          id: localStorage.getItem("code"),
          nombre: formData.nombre,
          correo: formData.email,
          rut: formData.rut,
          region: formData.region,
          cond1: terminos,
          cond2: spam

        }, function(response){

          swal({
     title: 'Registro Exitoso!',
     timer: 2000,
      type: 'success'
   }).then(
     function () {},
     // handling the promise rejection
     function (dismiss) {
       if (dismiss === 'timer') {
         console.log('I was closed by the timer')
       }
     }
   )

             $location.path('/Bienvenido/#feed');

        });
      }

      };

  });
