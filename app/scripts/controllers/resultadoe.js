'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:ResultadoeCtrl
 * @description
 * # ResultadoeCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('ResultadoeCtrl', function ($scope,$location,matchmedia) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.phone = matchmedia.isPhone();


    if($scope.phone){

      $scope.body = {

        'background-image':'url("../images/fondo_mix_cel.png")',
        'background-size': 'contain',
        'height': '100vh',
        'background-attachment':'fixed',
        'overflow':'hidden'
      }
}

else{
  $scope.body = {

    'background-image':'url("../images/fondo_mix.png")',
    'background-size': 'cover',
    'height': '100vh',
    'background-attachment':'fixed',
    'overflow':'hidden'
}

}

    var frase = localStorage.getItem("frase");
    var enero = localStorage.getItem("enero");
    var febrero =localStorage.getItem("febrero");

    var eneroentero = parseInt(enero.split('.')[0]);
    var febreroentero = parseInt(febrero.split('.')[0]);

    if (enero>febrero) {
    $scope.febrero = febreroentero + 1;
    $scope.enero = eneroentero;
    }
    else {
    $scope.enero = eneroentero + 1;
    $scope.febrero = febreroentero;
    }

    if (enero==febrero) {
        $scope.enero = eneroentero;
        $scope.febrero = febreroentero;
    }

    $scope.frase = frase;


    $scope.linktoregistro = function(){

      $location.path('/inscripcion-eneristas');
    };

    $scope.feed = function(){

      $location.path('/Bienvenido/#feed');
    }

  });
