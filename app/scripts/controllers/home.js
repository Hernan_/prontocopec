'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')

  .controller('HomeCtrl', function (Almacen,$location, conection, $scope,matchmedia) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    $scope.phone = matchmedia.isPhone();


    if($scope.phone){

      $scope.body = {

        'background-image':'url("../images/fondo_mix_cel.png")',
        'background-size': 'contain',
        'height': '300vh',
        'background-attachment':'fixed'
      }
}

else{
  $scope.body = {

    'background-image':'url("../images/fondo_mix.png")',
    'background-size': 'cover',
    'height': '300vh',
    'background-attachment':'fixed'
}

}



    $scope.entrada = function($el) {
      $el.addClass('animated fadeInRight'); // this example leverages animate.css classes
    };

    $scope.entradai = function($el) {
      $el.addClass('animated fadeInLeft'); // this example leverages animate.css classes
};

    $scope.GoToEnero = function(){
      $location.path('/eneristas');
    }


    $scope.GoToFebrero = function(){
      $location.path('/febreristas');
    }
  });
