'use strict';

/**
 * @ngdoc service
 * @name nuevoApp.conection
 * @description
 * # conection
 * Service in the nuevoApp.
 */
 //http://www.alafila.cl/code/Prontoctrl/
 //http://localhost/wsatencion/Prontoctrl/
angular.module('nuevoApp')

  .service('conection', function ($resource, $httpParamSerializerJQLike) {
    var servicio = $resource('http://www.alafila.cl/code/Prontoctrl/:action', null,
          {
    'porcentaje': { method:'POST',
                        params: { action : 'prontoCopec' },
                        headers : {"Content-Type": "application/x-www-form-urlencoded"},
                        transformRequest: function(data) {
                        return $httpParamSerializerJQLike(data);
                  }},
    'registro': { method:'POST',
                        params: { action : 'registro' },
                        headers : {"Content-Type": "application/x-www-form-urlencoded"},
                        transformRequest: function(data) {
                        return $httpParamSerializerJQLike(data);
                    }},
    'codeman': { method:'POST',
                        params: { action : 'generator' },
                        headers : {"Content-Type": "application/x-www-form-urlencoded"},
                        transformRequest: function(data) {
                        return $httpParamSerializerJQLike(data);
                    }},
      });

    return servicio;
  })


  .service('Almacen', function() {

    return {
        setData: function(nuevaData) {
          localStorage.cerro = JSON.stringify(nuevaData);
        },
        getData: function() {
          var data = null;
          try{
              data = JSON.parse(localStorage.cerro);
          } catch(e){
              data = null;
          }
          return data;
        },
        eraseData: function() {
          localStorage.cerro = null;
        }
    };
  });
