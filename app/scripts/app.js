'use strict';

/**
 * @ngdoc overview
 * @name nuevoApp
 * @description
 * # nuevoApp
 *
 * Main module of the application.
 */
angular
  .module('nuevoApp', [
    'ng-rut',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'LocalStorageModule',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularSmoothscroll',
    'angular-scroll-animate',
    'matchmedia-ng',
    'oitozero.ngSweetAlert'
  ])

.config(['localStorageServiceProvider', function(localStorageServiceProvider){

    localStorageServiceProvider.setPrefix('ls');
  }])


  .config(function ($routeProvider) {
    $routeProvider

      .when('/Bienvenido', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })

      .when('/eneristas', {
        templateUrl: 'views/eneristas.html',
        controller: 'EneristasCtrl',
        controllerAs: 'eneristas'
      })
      .when('/febreristas', {
        templateUrl: 'views/febreristas.html',
        controller: 'FebreristasCtrl',
        controllerAs: 'febreristas'
      })
      .when('/resultado-enerista', {
        templateUrl: 'views/resultadoe.html',
        controller: 'ResultadoeCtrl',
        controllerAs: 'resultadoe'
      })
      .when('/resultado-febrerista', {
        templateUrl: 'views/resultadof.html',
        controller: 'ResultadofCtrl',
        controllerAs: 'resultadof'
      })
      .when('/feed', {
        templateUrl: 'views/feed.html',
        controller: 'FeedCtrl',
        controllerAs: 'feed'
      })
      .when('/inscripcion-eneristas', {
        templateUrl: 'views/inscripcione.html',
        controller: 'InscripcioneCtrl',
        controllerAs: 'inscripcione'
      })
      .when('/inscripcion-febreristas', {
        templateUrl: 'views/inscripcionf.html',
        controller: 'InscripcionfCtrl',
        controllerAs: 'inscripcionf'
      })
      .otherwise({
        redirectTo: '/Bienvenido'
      });
  });
